const express = require('express');
const fs = require('fs');
const router = express.Router();

router.post('/', (req, res) => {
    const currentDate = new Date()
    const datetime = currentDate.toISOString()
    const fileName = `./messages/${datetime}.txt`;
    const messages = {...req.body, datetime}
    const json = JSON.stringify(messages)
    fs.writeFile(fileName, json, (err) => {
        if (err) {
            console.error(err);
        }
        console.log('File was saved!');
    });
    res.send('File was saved!');
});

router.get('/', (req, res) => {
    const path = "./messages";
    const arrMessages = [];
    fs.readdir(path, (err, files) => {
        if (err) throw err;
        files.forEach(file => {
            const str = fs.readFileSync(path + '/' + file);
            arrMessages.push(JSON.parse(str));
        });
        if (arrMessages.length > 5) {
            res.send(arrMessages.slice(-5));
        } else {
            res.send(arrMessages);
        }
    })
});


module.exports = router;